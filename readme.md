# Workflow optimization with Alfred

This repository contains the following workflows for alfred

* `matlab` - Matlab on a remote Server (X11 forwarding)
* `emacs` - EMacs (with Matlab integration if you install it) on a remote Server (X11 forwarding)
* `eth share` - Mount Netz Share
	
  To mount the share properly you need to be connected to the ETH Network or add a vpn network interface to OS X named VPN ETH
* `eth mystudies` - MyStudies Shortcut
* `eth gitlab d-phys` - D-Phys Gitlab Shortcut

In order to use them you must change the username tille in every workflow to your netz username